
import java.util.logging.Level;
import java.util.logging.Logger;

public class 优化入口 extends cc.hanzs.server.业务.__ {

    public static void main(String[] args) {
        try {
            //配置负载参數
            cc.hanzs.server.负载均衡.Property.i中心端口_i = 8080;

            //此rsa针对java客户端，可将密钥长度提高到很长
            cc.hanzs.server.负载均衡.Property.rsa = cc.hanzs.安全.RSA.d副本(1);
            cc.hanzs.server.负载均衡.Property.rsa.g置私钥(cc.hanzs.编码.Base64.d解码.decode("MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAvnU1UYcd93QJv9VLZuCAPONa9QBL5HaiJEkmecnAZZl0QUsrnVYIUF9uIdQu538YVHhFgdXpgF+PKWQ6NkLcfwIDAQABAkEAoYQQlXU6qpHTesXNzg7xcbF1f3Si57rbwl3urTW1XfKNuoRj9eNW1hvii67V61/6TTdqTQ/VBqVSMNB7sOLQQQIhAPo+JJY78fhXr3uWJ0Hvp8GjVk3kOU6Pm5IDx/5SO/AxAiEAwtbzHMDqjJHuIvLrm5q0nB6Os7WzhAlcWyhX3gecm68CIFUpFsxlxBfux1J4rA19YIq+al2BoJsCtj+/t6zuEWXRAiEAqlYfPoENzQz7YwR6pJPw2BgC6+urYICKhItVnh+Lc7MCIHhkYzuaBPF9DrMj4sNwxiQCxrRHC2iXVmI/5fVw/QWE"));
//            cc.hanzs.server.负载均衡.Property.rsa.g置公钥(cc.hanzs.编码.Base64.d解码.decode("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAL51NVGHHfd0Cb/VS2bggDzjWvUAS+R2oiRJJnnJwGWZdEFLK51WCFBfbiHULud/GFR4RYHV6YBfjylkOjZC3H8CAwEAAQ=="));

            //此rsa针对c#客户端，固定长度为1024
            //JAVA:
            //公钥:MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRhc5tk4nJzI3WcDZNDuUjtrVF2IHpBUeW5FMMwdrEYlzqIMjOny4f2GshlDqKfl21Xevj3y0/khhCeKLQp+5cDAafKDXj39GZmG7HKeWZp+DC8AErVYsQCRaXAJuzuowmmunbOa4XoOBfF3aG8o54vz1Tty2MfZpDAur0mUdWbwIDAQAB
            //私钥:MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJGFzm2TicnMjdZwNk0O5SO2tUXYgekFR5bkUwzB2sRiXOogyM6fLh/YayGUOop+XbVd6+PfLT+SGEJ4otCn7lwMBp8oNePf0ZmYbscp5Zmn4MLwAStVixAJFpcAm7O6jCaa6ds5rheg4F8Xdobyjni/PVO3LYx9mkMC6vSZR1ZvAgMBAAECgYAUbIEyY9ic1/a4N/uHCSYjwwriCKcC+1i21sjJK5P2qA9+VKeaSEEC+y7eAZ3NvjqMr+fhjXseRvTcr226dzc+wcGsxkJGWq3uTQa1od4yY1Mu5SRX7OnQ1wnYuIxhgt3+pMtjCe/JbAAZOd+GyDp4t2LJdwPRwOLKzIGmuBb+4QJBAMhxrG0UuQDGreviM1yIwrK/OFNN4EJunGWyrmi23YWxvBIFiGV0IEXRxpXKW/lAMGyQ3q2z9NQjfq/vrkq7EVkCQQC52zzELHNDkHzR3yow5SeEXfyafrdrAyGe+UM0KyOZyUkAvHj6VV7z7Kl8JAzsBdZFU/ujhJeysGNgYlSs6CUHAkEAkI5qa8CmK4kdpSBdKj73KzH8zRc8xKxA3pcecSZwXBryJ40V8RTusWu4bA6khf1k7ucLsPo3d0ah9wBEEmEXAQJAVJ1gq/uEupT0juQoJDyf4h6cG4ZnYRv62ZKoKv8c5S4Dw4cLGQV3BlyPyQnsrCP4KnYS0Z5TJjIx/DaoUFQcTwJAPBkI6jnZ5WzhfGmLfjqzsteICdY3OAO+E+Bsxu2MU3JFGXctu+MW1G+M2+2qZ4jJOsz3WQTY77ZOaH78Ja6kHA==
            //
            //C#:
            //公钥为：AQAB
            //模为：kYXObZOJycyN1nA2TQ7lI7a1RdiB6QVHluRTDMHaxGJc6iDIzp8uH9hrIZQ6in5dtV3r498tP5IYQnii0KfuXAwGnyg149/RmZhuxynlmafgwvABK1WLEAkWlwCbs7qMJprp2zmuF6DgXxd2hvKOeL89U7ctjH2aQwLq9JlHVm8=
            cc.hanzs.server.负载均衡.Property.RsaForDot = cc.hanzs.安全.RSA.d副本(2);
            cc.hanzs.server.负载均衡.Property.RsaForDot.g置私钥(cc.hanzs.编码.Base64.d解码.decode("MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJGFzm2TicnMjdZwNk0O5SO2tUXYgekFR5bkUwzB2sRiXOogyM6fLh/YayGUOop+XbVd6+PfLT+SGEJ4otCn7lwMBp8oNePf0ZmYbscp5Zmn4MLwAStVixAJFpcAm7O6jCaa6ds5rheg4F8Xdobyjni/PVO3LYx9mkMC6vSZR1ZvAgMBAAECgYAUbIEyY9ic1/a4N/uHCSYjwwriCKcC+1i21sjJK5P2qA9+VKeaSEEC+y7eAZ3NvjqMr+fhjXseRvTcr226dzc+wcGsxkJGWq3uTQa1od4yY1Mu5SRX7OnQ1wnYuIxhgt3+pMtjCe/JbAAZOd+GyDp4t2LJdwPRwOLKzIGmuBb+4QJBAMhxrG0UuQDGreviM1yIwrK/OFNN4EJunGWyrmi23YWxvBIFiGV0IEXRxpXKW/lAMGyQ3q2z9NQjfq/vrkq7EVkCQQC52zzELHNDkHzR3yow5SeEXfyafrdrAyGe+UM0KyOZyUkAvHj6VV7z7Kl8JAzsBdZFU/ujhJeysGNgYlSs6CUHAkEAkI5qa8CmK4kdpSBdKj73KzH8zRc8xKxA3pcecSZwXBryJ40V8RTusWu4bA6khf1k7ucLsPo3d0ah9wBEEmEXAQJAVJ1gq/uEupT0juQoJDyf4h6cG4ZnYRv62ZKoKv8c5S4Dw4cLGQV3BlyPyQnsrCP4KnYS0Z5TJjIx/DaoUFQcTwJAPBkI6jnZ5WzhfGmLfjqzsteICdY3OAO+E+Bsxu2MU3JFGXctu+MW1G+M2+2qZ4jJOsz3WQTY77ZOaH78Ja6kHA=="));
//            cc.hanzs.server.负载均衡.Property.rsa.g置公钥(cc.hanzs.编码.Base64.d解码.decode("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRhc5tk4nJzI3WcDZNDuUjtrVF2IHpBUeW5FMMwdrEYlzqIMjOny4f2GshlDqKfl21Xevj3y0/khhCeKLQp+5cDAafKDXj39GZmG7HKeWZp+DC8AErVYsQCRaXAJuzuowmmunbOa4XoOBfF3aG8o54vz1Tty2MfZpDAur0mUdWbwIDAQAB"));

            //启动负载均衡
//            Main d = new Main();
            cc.hanzs.server.负载均衡.负载.init(new 优化入口());
            cc.hanzs.server.负载均衡.负载.start();
            System.out.println("启动完毕");
        } catch (java.io.IOException | java.security.NoSuchAlgorithmException | java.security.spec.InvalidKeySpecException ex) {
            Logger.getLogger(优化入口.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    @Override
    public byte[] g处理请求(final int session_i, final byte[] ci客户数据_byteArray) {
        cc.hanzs.json.JSONObject ji_JSON = cc.hanzs.json.JSONObject.d副本("{success:true,_:'测试'}");
        ji_JSON.put("@", session_i);
        return ji_JSON.toString().getBytes();
    }

    @Override
    public void removeSession(int session_i) {//移除用户
    }

    @Override
    public int get用户数量_i() {//返回注册用户数量
        return 20;
    }
}
