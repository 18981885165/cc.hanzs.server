package cc.hanzs.server.负载均衡;

public class Session implements Cloneable {

    private final static Session session = new Session();
    public final static long ii时限_l = 60_000;//在规定时间段内未进行会晤，则认为客户端已经断开，清除所有相关资源。默认1′
    public byte[] im密钥_byte1s = null;//只有密钥，未限定加密算法，为将来预留其他加密算法。缺点是降低运行速度
    public long iz最近访问时间_l = Long.MAX_VALUE;
    public String iIP_s = null;
    public static int IP峰值_i = 1;
    public boolean iv正在服务_b = false;//用来强制客户端只能串行申请服务
    public final static int if访问频率峰值_i = 50, if访问频率递减梯度_i = 10;
    public int if访问频率_i = 0;//用来控制客户端的恶意访问。比如客户自己写一个软件自动访问，进而窃取公司数据。访问频率的限制能降低数据泄密速度，但不能禁止
    public final static int ij接收数据量峰值_i = 9600, ij接收数据量递减梯度_i = 500;
    public int ij接收数据量_i = 0;//用来控制客户端的恶意访问。比如客户自己写一个软件进行DDOS（拒绝服务）攻击，用于控制客户端发送大量数据，佔用资源，影响正常用户的服务申请。单位：字节，每5″减去相应梯度
    public final static int id递回数据量峰值_i = 12000, id递回数据量递减梯度_i = 1200;
    public int id递回数据量_i = 0;//用来控制客户端的恶意访问。比如客户自己写一个软件进行DDOS（拒绝服务）攻击，用于控制客户端刻意申请大数据量服务，佔用资源，影响正常用户的服务申请。单位：字节，每5″减去相应梯度
    public int iy映射号_i = 0;
    public byte ij加密算法_byte = 0;

    private Session() {
    }

    public static Session d副本() {
        try {
            return (Session) session.clone();
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }

    public void close() {
        iIP_s = null;
        im密钥_byte1s = null;
    }
}
