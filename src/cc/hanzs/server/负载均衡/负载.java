package cc.hanzs.server.负载均衡;

public final class 负载 {

    private static class I {

        //相当于常量
        static final int Uih缓冲区容量_i = 9600;//考虑XP系统，缓冲区限定容量为9K。
        static final java.util.Random Udb不确定數 = new java.util.Random();
        static byte[] Uik空返回值_byte1s = null;
        static java.util.concurrent.ExecutorService dx线程池 = null;
        static final byte ii申请服务_byte = 0, iw维持连接_byte = 2, it退出_byte = 3;
        static final byte iw未压缩_byte = 0, iGZIP压缩_byte = 1, iLZMA压缩_byte = 2;
        static final byte iw未加密_byte = 0, iAES加密算法_byte = 1;
        static String in内网IP_s = null, ig公网IP_s = null;
        static cc.hanzs.server.业务.__ dy业务 = null;

        //变量
        static final java.util.TreeMap<String, Integer> IP_MAP = new java.util.TreeMap<>();
        static final java.util.TreeMap<Integer, cc.hanzs.server.负载均衡.Session> iy映射_MAP = new java.util.TreeMap<>();
        static java.nio.channels.Selector dj监聽selector = null;
        static final java.util.TreeMap<String, byte[]> i发缓冲_Map = new java.util.TreeMap<>();
    }

    public static void init(final cc.hanzs.server.业务.__ cd业务) throws java.io.IOException {
        init_本地IP();
        init_公网IP();

        (new 清理映射池()).start();

        //设置线程池
        int ji线程數量_i = Runtime.getRuntime().availableProcessors() * 2 - 2;//监听、清理各占一个线程。考虑到网路通信比较慢，
        if (ji线程數量_i > 1) {
            I.dx线程池 = java.util.concurrent.Executors.newFixedThreadPool(ji线程數量_i);
        }

        I.dy业务 = cd业务;
        Session.IP峰值_i = cd业务.get用户数量_i() * 2;//设定IP使用次数的峰值，预防同一IP不停的与服务器握手，造成DOS攻击。默认注册用户数量的2倍。
        I.Uik空返回值_byte1s = "{success:false,_:''}".getBytes("UTF-8");
        I.Uik空返回值_byte1s = java.util.Arrays.copyOf(I.Uik空返回值_byte1s, I.Uik空返回值_byte1s.length + 1);
        System.arraycopy(I.Uik空返回值_byte1s, 0, I.Uik空返回值_byte1s, 1, I.Uik空返回值_byte1s.length - 1);
        I.Uik空返回值_byte1s[0] = 0;

        I.dj监聽selector = java.nio.channels.Selector.open();// 通过open()方法找到Selector

        //注册监聽握手
        java.nio.channels.ServerSocketChannel serverSocketChannel;
        serverSocketChannel = java.nio.channels.ServerSocketChannel.open();// 打开服务器套接字通道
        serverSocketChannel.configureBlocking(false);// 服务器配置为非阻塞
        serverSocketChannel.socket().bind(new java.net.InetSocketAddress(cc.hanzs.server.负载均衡.Property.i中心端口_i));// 进行服务的绑定
        serverSocketChannel.register(I.dj监聽selector, java.nio.channels.SelectionKey.OP_ACCEPT);// 注册到selector，等待连接

        //注册监听业务
        serverSocketChannel = java.nio.channels.ServerSocketChannel.open();// 打开服务器套接字通道
        serverSocketChannel.configureBlocking(false);// 服务器配置为非阻塞
        serverSocketChannel.socket().bind(new java.net.InetSocketAddress(cc.hanzs.server.负载均衡.Property.i中心端口_i + 2));// 进行服务的绑定
        serverSocketChannel.register(I.dj监聽selector, java.nio.channels.SelectionKey.OP_ACCEPT);// 注册到selector，等待连接
    }

    private static void init_本地IP() throws java.net.SocketException {
        java.util.Enumeration<java.net.NetworkInterface> mEnumeration;

        mEnumeration = java.net.NetworkInterface.getNetworkInterfaces();
        while (mEnumeration.hasMoreElements()) {
            java.net.NetworkInterface intf = mEnumeration.nextElement();
            for (java.util.Enumeration<java.net.InetAddress> enumIPAddr = intf.getInetAddresses(); enumIPAddr.hasMoreElements();) {
                java.net.InetAddress inetAddress = enumIPAddr.nextElement();
                // 如果不是回环地址  
                if (!inetAddress.isLoopbackAddress()) {
                    // 直接返回本地IP地址  
                    I.in内网IP_s = inetAddress.getHostAddress();
                    if (I.in内网IP_s.contains(":")) {
                        I.in内网IP_s = null;//不采用IPV6
                    } else {
                        return;
                    }
                }
            }
        }
        //
        if (I.in内网IP_s == null) {
            I.in内网IP_s = "127.0.0.1";
        }
    }

    private static void init_公网IP() throws java.net.SocketException {
        java.util.Enumeration<java.net.NetworkInterface> netInterfaces;
        java.net.InetAddress ip;
        java.util.Enumeration<java.net.InetAddress> address;

        netInterfaces = java.net.NetworkInterface.getNetworkInterfaces();
        while (netInterfaces.hasMoreElements()) {
            java.net.NetworkInterface ni = netInterfaces.nextElement();
            address = ni.getInetAddresses();
            while (address.hasMoreElements()) {
                ip = address.nextElement();
                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && !ip.getHostAddress().contains(":")) {// 外网IP
                    I.ig公网IP_s = ip.getHostAddress();
                    if (I.ig公网IP_s.contains(":")) {
                        I.ig公网IP_s = null;//不采用IPV6
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public static void start() {
        (new 监聽请求()).start();
    }

    private static final class 监聽请求 extends java.lang.Thread {

        @Override
        public void run() {
            // <editor-fold defaultstate="collapsed" desc="自用">
            class 自用 {

                java.util.Set<java.nio.channels.SelectionKey> selectionKeys = null;
                java.util.Iterator<java.nio.channels.SelectionKey> iterator = null;
                java.nio.channels.SelectionKey selectionKey = null;
                java.nio.channels.ServerSocketChannel server = null;
                java.nio.channels.SocketChannel client = null;
                业务访问 dj监聽客户端业务访问 = null;
                握手 dj监聽客户端握手 = null;
                java.nio.ByteBuffer if发送缓冲区 = java.nio.ByteBuffer.allocate(I.Uih缓冲区容量_i);
                byte[] i_byte1s = null;
                int ib被访问端口_i;

                void clear() {
                    selectionKeys = null;
                    iterator = null;
                    selectionKey = null;
                    server = null;
                    client = null;
                    i_byte1s = null;
                    dj监聽客户端握手 = null;
                    dj监聽客户端业务访问 = null;
                }
            }// </editor-fold>
            自用 z = new 自用();
            for (;;) {
                try {
                    z.clear();

                    I.dj监聽selector.select();
                    z.selectionKeys = I.dj监聽selector.selectedKeys();
                    z.iterator = z.selectionKeys.iterator();
                    while (z.iterator.hasNext()) {
                        z.selectionKey = z.iterator.next();
                        z.iterator.remove();
                        if (z.selectionKey.isAcceptable()) {//开始连接
                            z.server = (java.nio.channels.ServerSocketChannel) z.selectionKey.channel();
                            z.client = z.server.accept();
                            z.client.configureBlocking(false);
                            z.client.register(I.dj监聽selector, java.nio.channels.SelectionKey.OP_READ);
                        } else if (z.selectionKey.isReadable()) {
                            z.client = (java.nio.channels.SocketChannel) z.selectionKey.channel();
                            z.ib被访问端口_i = z.client.socket().getLocalPort();
                            if (z.ib被访问端口_i == cc.hanzs.server.负载均衡.Property.i中心端口_i) {//注册监聽客户端握手
                                z.dj监聽客户端握手 = 握手.d副本(z.client, I.dj监聽selector, z.selectionKey);
                                if (z.dj监聽客户端握手 != null) {
                                    if (I.dx线程池 == null) {
                                        z.dj监聽客户端握手.run();
                                    } else {
                                        I.dx线程池.execute(z.dj监聽客户端握手);
                                    }
                                }
                            } else {
                                z.dj监聽客户端业务访问 = 业务访问.d副本(z.client, I.dj监聽selector, z.selectionKey);
                                if (z.dj监聽客户端业务访问 != null) {
                                    if (I.dx线程池 == null) {
                                        z.dj监聽客户端业务访问.run();
                                    } else {
                                        I.dx线程池.execute(z.dj监聽客户端业务访问);
                                    }
                                }
                            }
                        } else if (z.selectionKey.isWritable()) {
                            z.client = (java.nio.channels.SocketChannel) z.selectionKey.channel();
                            z.i_byte1s = I.i发缓冲_Map.remove(z.client.toString());
                            if (z.i_byte1s != null) {
                                z.if发送缓冲区.clear();
                                z.if发送缓冲区.put(z.i_byte1s);
                                z.if发送缓冲区.flip();
                                try {
                                    z.client.write(z.if发送缓冲区);
                                } catch (java.io.IOException ex) {
                                } finally {
                                    try {
                                        z.client.close();
                                    } catch (java.io.IOException ex1) {
                                    }
                                    z.selectionKey.cancel();
                                }
                            }
                        }
                    }
                    z.selectionKeys.clear();
                } catch (java.io.IOException | java.nio.channels.CancelledKeyException ex) {
                    try {
                        if (z.client != null) {
                            z.client.close();
                        }
                    } catch (java.io.IOException ex1) {
                    }
                    if (z.selectionKey != null) {
                        z.selectionKey.cancel();
                    }
                }
            }
        }
    }

    private static final class 握手 implements Cloneable, Runnable {

        private static final 握手 d握手 = new 握手();
        private java.nio.channels.SelectionKey selectionKey = null;
        private java.nio.channels.SocketChannel client = null;
        private byte[] i_byte1s = null;

        private 握手() {
        }

        public static 握手 d副本(final java.nio.channels.SocketChannel cdclient, final java.nio.channels.Selector cdj监聽selector, final java.nio.channels.SelectionKey cdselectionKey) {
            握手 jd;
            byte[] ji_byte1s = null;
            try {

                //检测某个IP出现攻击行为。
                String IP_s = cdclient.socket().getInetAddress().toString();
                int IP_i = 0;
                Integer IP_I = I.IP_MAP.get(IP_s);
                if (IP_I != null) {
                    IP_i = IP_I;
                }
                if (IP_i >= Session.IP峰值_i) {//若此IP使用次数超过峰值（认为受到攻击），不予握手
                    cdclient.close();
                    cdselectionKey.cancel();
                    return null;
                }
                jg增加or减少IP次数(IP_s, true);

                ji_byte1s = 握手.jgj接收数据_byte1s(cdselectionKey);
                if (ji_byte1s == null) {
                    try {
                        cdclient.close();
                    } catch (java.io.IOException ex) {
                    }
                    cdselectionKey.cancel();
                    return null;
                } else {
                    jd = (握手) d握手.clone();
                    jd.selectionKey = cdselectionKey;
                    jd.client = cdclient;
                    jd.i_byte1s = ji_byte1s;
                    cdclient.register(cdj监聽selector, java.nio.channels.SelectionKey.OP_WRITE);
                    return jd;
                }
            } catch (java.io.IOException | CloneNotSupportedException ex) {//处理客户端意外非正常关闭情况
                return null;
            } finally {
            }
        }

        @Override
        public void run() {
            try {
                i_byte1s = jgdy用私钥解密_byte1s(i_byte1s);
                if (i_byte1s == null) {//解密失败，则直接断开连接
                    client.close();
                    selectionKey.cancel();
                    return;
                }

                Integer jiy映射号_I = jgdi生成映射_I(i_byte1s);
                if (jiy映射号_I == null) {
                    client.close();
                    selectionKey.cancel();
                    return;
                }

                i_byte1s = jgdd递出_byte1s(jiy映射号_I);
                I.i发缓冲_Map.put(client.toString(), i_byte1s);
            } catch (java.io.IOException ex) {
            } finally {
                i_byte1s = null;
                selectionKey = null;
            }
        }

        private static byte[] jgj接收数据_byte1s(final java.nio.channels.SelectionKey SelectionKey) {
            // <editor-fold defaultstate="collapsed" desc="自用">
            class 自用 implements Cloneable {

                java.nio.channels.SocketChannel client = null;
                java.nio.ByteBuffer i接收缓冲区 = null;
                byte[] i_byte1s = null;
                int cont;

                自用 d副本() throws CloneNotSupportedException {
                    自用 jd = (自用) super.clone();
                    jd.i接收缓冲区 = java.nio.ByteBuffer.allocate(I.Uih缓冲区容量_i);
                    return jd;
                }

                void close() {
                    i接收缓冲区 = null;
                    i_byte1s = null;
                }
            }// </editor-fold>
            自用 z = null;
            try {
                // <editor-fold defaultstate="collapsed" desc="利用对象复制初始化过程内部变量">
                z = (自用) cc.hanzs.常用.d对象池.get(自用.class.getName());
                if (z == null) {
                    z = new 自用();
                    cc.hanzs.常用.d对象池.put(自用.class.getName(), z);
                }
                z = z.d副本();
                // </editor-fold>
                z.client = (java.nio.channels.SocketChannel) SelectionKey.channel();
                z.cont = z.client.read(z.i接收缓冲区);
                z.i_byte1s = z.i接收缓冲区.array();
                z.i_byte1s = java.util.Arrays.copyOf(z.i_byte1s, z.cont);
                return z.i_byte1s;
            } catch (java.io.IOException | CloneNotSupportedException ex) {//处理客户端意外非正常关闭情况或非NIO连接情况
                return null;
            } finally {
                // <editor-fold defaultstate="collapsed" desc="释放资源">
                if (z != null) {
                    z.close();
                }
                // </editor-fold>
            }
        }

        /**
         * 考虑到将来用C#写客户端，所以先用RsaForDot解密，解密失败在试着用rsa解密
         */
        private byte[] jgdy用私钥解密_byte1s(final byte[] ci_byte1s) {
            try {
                return cc.hanzs.server.负载均衡.Property.RsaForDot.i用私钥解密_byteArray(ci_byte1s);
            } catch (java.security.NoSuchAlgorithmException | javax.crypto.NoSuchPaddingException | java.security.InvalidKeyException | javax.crypto.IllegalBlockSizeException | javax.crypto.BadPaddingException | java.io.IOException ex) {
                try {
                    return cc.hanzs.server.负载均衡.Property.rsa.i用私钥解密_byteArray(ci_byte1s);
                } catch (java.security.NoSuchAlgorithmException | javax.crypto.NoSuchPaddingException | java.security.InvalidKeyException | javax.crypto.IllegalBlockSizeException | javax.crypto.BadPaddingException | java.io.IOException ex1) {
                    return null;
                }
            }
        }

        private Integer jgdi生成映射_I(byte[] ci_byte1s) {
            // <editor-fold defaultstate="collapsed" desc="自用">
            class 自用 extends cc.hanzs.Close implements Cloneable {

                cc.hanzs.server.负载均衡.Session iSession新 = null, iSession旧 = null;
                int iy映射号_i;
                long id当前时间_l;

                自用 d副本() throws CloneNotSupportedException {
                    自用 jd = (自用) super.clone();
                    return jd;
                }
            }// </editor-fold>
            自用 z = null;
            try {
                // <editor-fold defaultstate="collapsed" desc="利用对象复制初始化过程内部变量">
                z = (自用) cc.hanzs.常用.d对象池.get(自用.class.getName());
                if (z == null) {
                    z = new 自用();
                    cc.hanzs.常用.d对象池.put(自用.class.getName(), z);
                }
                z = z.d副本();
                // </editor-fold>

                //建立系统内部会晤
                switch (ci_byte1s[0]) {
                    case I.iw未加密_byte:
                        z.iSession新 = cc.hanzs.server.负载均衡.Session.d副本();
                        z.iSession新.ij加密算法_byte = ci_byte1s[0];
                        break;
                    case I.iAES加密算法_byte:
                        z.iSession新 = cc.hanzs.server.负载均衡.Session.d副本();
                        z.iSession新.ij加密算法_byte = ci_byte1s[0];
                        System.arraycopy(ci_byte1s, 1, ci_byte1s, 0, ci_byte1s.length - 1);
                        z.iSession新.im密钥_byte1s = java.util.Arrays.copyOf(ci_byte1s, ci_byte1s.length - 1);
                        break;
                    default:
                        return null;//发现未知加密方式，不予握手
                }

                for (;;) {
                    z.iy映射号_i = I.Udb不确定數.nextInt();
                    z.iSession旧 = I.iy映射_MAP.get(z.iy映射号_i);
                    if (z.iSession旧 != null) {//存在旧会晤，先移除
                        清理(z.iSession旧);
                        break;
                    } else {
                        break;
                    }
                }
                z.iSession新.iIP_s = client.socket().getInetAddress().toString();
                z.iSession新.iy映射号_i = z.iy映射号_i;
                I.iy映射_MAP.put(z.iy映射号_i, z.iSession新);
                return z.iy映射号_i;
            } catch (CloneNotSupportedException ex) {
                return null;
            } finally {
                // <editor-fold defaultstate="collapsed" desc="释放资源">
                if (z != null) {
                    z.close();
                }
                // </editor-fold>
            }
        }

        private byte[] jgdd递出_byte1s(final int ciy映射号_i) {
            // <editor-fold defaultstate="collapsed" desc="递出">
            class 自用 extends cc.hanzs.Close implements Cloneable {

                cc.hanzs.server.负载均衡.Session iSession = null;
                byte[] i_byte1s = null;
                cc.hanzs.json.JSONObject i_JSON = null;

                自用 d副本() throws CloneNotSupportedException {
                    自用 jd = (自用) super.clone();
                    return jd;
                }
            }// </editor-fold>
            自用 z = null;
            try {
                // <editor-fold defaultstate="collapsed" desc="利用对象复制初始化过程内部变量">
                z = (自用) cc.hanzs.常用.d对象池.get(自用.class.getName());
                if (z == null) {
                    z = new 自用();
                    cc.hanzs.常用.d对象池.put(自用.class.getName(), z);
                }
                z = z.d副本();
                // </editor-fold>

                z.iSession = I.iy映射_MAP.get(ciy映射号_i);
                z.i_JSON = cc.hanzs.json.JSONObject.d副本();
                z.i_JSON.put("公网IP_s", I.ig公网IP_s);
                z.i_JSON.put("客户端IP_s", client.socket().getInetAddress().toString());
                z.i_JSON.put("内网IP_s", I.in内网IP_s);
                z.i_JSON.put("会晤号_i", ciy映射号_i);
                z.i_byte1s = z.i_JSON.toString().getBytes("UTF-8");
                z.i_byte1s = jgd对称加密(z.i_byte1s, z.iSession);
                return z.i_byte1s;
            } catch (CloneNotSupportedException | java.io.UnsupportedEncodingException ex) {
                return null;
            } finally {
            }
        }

        private byte[] jgd对称加密(final byte[] ci_byte1s, final cc.hanzs.server.负载均衡.Session session) {
//            return cc.hanzs.安全.AES.i加密_byteArray(ci_byte1s, AES_Key);
            if (session.im密钥_byte1s == null) {
                return ci_byte1s;
            }
            switch (session.ij加密算法_byte) {
                case I.iAES加密算法_byte:
                    java.security.Key AES_Key = new javax.crypto.spec.SecretKeySpec(session.im密钥_byte1s, "AES");
                    return cc.hanzs.安全.AES.i加密_byteArray(ci_byte1s, AES_Key);
                default:
                    return ci_byte1s;
            }
        }
    }

    private static class 业务访问 implements Cloneable, Runnable {

        private static final 业务访问 d业务访问 = new 业务访问();
        private java.nio.channels.SelectionKey selectionKey = null;
        private java.nio.channels.SocketChannel client = null;
        private byte[] i_byte1s = null;

        public static byte[] g接收数据(final java.nio.channels.SelectionKey SelectionKey) {
            // <editor-fold defaultstate="collapsed" desc="自用">
            class 自用 implements Cloneable {

                java.nio.channels.SocketChannel client = null;
                java.nio.ByteBuffer i接收缓冲区 = null;
                byte[] i_byte1s = null;
                int cont;

                自用 d副本() throws CloneNotSupportedException {
                    自用 jd = (自用) super.clone();
                    jd.i接收缓冲区 = java.nio.ByteBuffer.allocate(I.Uih缓冲区容量_i);
                    return jd;
                }

                void close() {
                    i接收缓冲区 = null;
                    i_byte1s = null;
                }
            }// </editor-fold>
            自用 z = null;
            try {
                // <editor-fold defaultstate="collapsed" desc="利用对象复制初始化过程内部变量">
                z = (自用) cc.hanzs.常用.d对象池.get(自用.class.getName());
                if (z == null) {
                    z = new 自用();
                    cc.hanzs.常用.d对象池.put(自用.class.getName(), z);
                }
                z = z.d副本();
                // </editor-fold>
                z.client = (java.nio.channels.SocketChannel) SelectionKey.channel();
                z.cont = z.client.read(z.i接收缓冲区);
                z.i_byte1s = z.i接收缓冲区.array();
                z.i_byte1s = java.util.Arrays.copyOf(z.i_byte1s, z.cont);
                return z.i_byte1s;
            } catch (java.io.IOException | CloneNotSupportedException ex) {//处理客户端意外非正常关闭情况或非NIO连接情况
                return null;
            } finally {
                // <editor-fold defaultstate="collapsed" desc="释放资源">
                if (z != null) {
                    z.close();
                }
                // </editor-fold>
            }
        }

        private static cc.hanzs.server.负载均衡.Session jgy由映射号获得映射(final int ci_i) {
            return I.iy映射_MAP.get(ci_i);
        }

        private static byte[] jgj解密_byte1s(final byte[] ci_byte1s, final cc.hanzs.server.负载均衡.Session session) {
            if (session.im密钥_byte1s == null) {
                return ci_byte1s;
            }
            switch (session.ij加密算法_byte) {
                case I.iAES加密算法_byte:
                    java.security.Key AES_Key = new javax.crypto.spec.SecretKeySpec(session.im密钥_byte1s, "AES");
                    return cc.hanzs.安全.AES.i解密_byteArray(ci_byte1s, AES_Key);
                default:
                    return null;
            }
        }

        private static byte[] jgj解压_byte1s(final byte[] ci_byte1s) {
            byte[] ji_byte1s;
            int ji压缩方法_i = ci_byte1s[0];

            //截去压缩方法
            ji_byte1s = ci_byte1s.clone();
            System.arraycopy(ji_byte1s, 1, ji_byte1s, 0, ji_byte1s.length - 1);
            ji_byte1s = java.util.Arrays.copyOf(ji_byte1s, ji_byte1s.length - 1);

            //解压
            switch (ji压缩方法_i) {
                case I.iw未压缩_byte:
                    break;
                case I.iGZIP压缩_byte:
                    ji_byte1s = cc.hanzs.压缩解压.Gzip.i解压_byteArray(ji_byte1s, 0.01);//默认压缩率最高1%。否则默认遇到压缩包炸弹。
                    break;
                case I.iLZMA压缩_byte:
                    ji_byte1s = cc.hanzs.压缩解压.Lzma.i解压_byteArray(ji_byte1s, 0.01);//默认压缩率最高1%。否则默认遇到压缩包炸弹。
                    break;
            }
            return ji_byte1s;
        }

        private static byte[] jgy压缩_byte1s(final byte[] ci_byte1s) {
            byte[] ji_byte1s = null, ji1_byte1s;
            ji1_byte1s = ci_byte1s.clone();

            //压缩
            if (ji1_byte1s.length >= 200) {//待压缩字节数量＜200，无需压缩。服务端只采用GZIP压缩，寻得压缩率与压缩耗时的平衡
                ji_byte1s = cc.hanzs.压缩解压.Gzip.i压缩_byteArray(ji1_byte1s);
                if (ji_byte1s.length < ji1_byte1s.length) {
                    ji1_byte1s = ji_byte1s;
                } else {
                    ji_byte1s = null;
                }
            }

            //插入压缩方式
            ji1_byte1s = java.util.Arrays.copyOf(ji1_byte1s, ji1_byte1s.length + 1);
            System.arraycopy(ji1_byte1s, 0, ji1_byte1s, 1, ji1_byte1s.length - 1);
            if (ji_byte1s == null) {
                ji1_byte1s[0] = I.iw未压缩_byte;
            } else {
                ji1_byte1s[0] = I.iGZIP压缩_byte;
            }

            return ji1_byte1s;
        }

        private static byte[] jgj加密_byte1s(final byte[] ci_byte1s, final cc.hanzs.server.负载均衡.Session session) {
            if (session.im密钥_byte1s == null) {
                return ci_byte1s;
            }
            switch (session.ij加密算法_byte) {
                case I.iAES加密算法_byte:
                    java.security.Key AES_Key = new javax.crypto.spec.SecretKeySpec(session.im密钥_byte1s, "AES");
                    return cc.hanzs.安全.AES.i加密_byteArray(ci_byte1s, AES_Key);
                default:
                    return ci_byte1s;
            }
        }

        public static 业务访问 d副本(final java.nio.channels.SocketChannel client, final java.nio.channels.Selector cdj监聽selector, final java.nio.channels.SelectionKey cdselectionKey) {
            业务访问 jd;
            try {
                jd = (业务访问) d业务访问.clone();
                jd.selectionKey = cdselectionKey;
                jd.client = client;
                jd.i_byte1s = 业务访问.g接收数据(cdselectionKey);
                if (jd.i_byte1s == null || jd.i_byte1s.length == 0) {
                    client.close();
                    cdselectionKey.cancel();
                    return null;
                } else {
                    client.register(cdj监聽selector, java.nio.channels.SelectionKey.OP_WRITE);
                    return jd;
                }
//                else {
//                    switch (jd.i_byte1s[0]) {
//                        case I.iv注销连接_i:
//                            jg注销连接(cdselectionKey);
//                            return null;
//                        case I.ij建立连接_i://建立连接，获取连接数
//                        case I.is申请服务_i:
//                            jd.selectionKey = cdselectionKey;
//
//                            client = (java.nio.channels.SocketChannel) cdselectionKey.channel();
//                            client.register(cdj监聽selector, java.nio.channels.SelectionKey.OP_WRITE);
//                            return jd;
//                        default:
//                            return null;
//                    }
//                }
            } catch (CloneNotSupportedException | java.io.IOException ex) {
                return null;
            }
        }

        @Override
        public void run() {
            // <editor-fold defaultstate="collapsed" desc="自用">
            class 自用 implements Cloneable {

                java.nio.channels.SocketChannel client = null;
                cc.hanzs.server.负载均衡.Session dy映射_Session = null;
                byte iv指令_byte;
                int iy映射号_i;
                byte[] il临时_byte1s = null, i_byte1s = null;

                自用 d副本() throws CloneNotSupportedException {
                    自用 jd = (自用) super.clone();
                    return jd;
                }

                void close() {
                    client = null;
//                    i发送缓冲区 = null;
                }
            }// </editor-fold>
            自用 z = null;
            try {
                // <editor-fold defaultstate="collapsed" desc="利用对象复制初始化过程内部变量">
                z = (自用) cc.hanzs.常用.d对象池.get(自用.class.getName());
                if (z == null) {
                    z = new 自用();
                    cc.hanzs.常用.d对象池.put(自用.class.getName(), z);
                }
                z = z.d副本();
                // </editor-fold>
                z.client = (java.nio.channels.SocketChannel) selectionKey.channel();
                z.i_byte1s = i_byte1s.clone();

                //获取映射号
                z.il临时_byte1s = new byte[4];
                System.arraycopy(z.i_byte1s, 0, z.il临时_byte1s, 0, 4);
                z.iy映射号_i = cc.hanzs.lang.转换.byteArray_2_int(z.il临时_byte1s);
                ///获得映射
                z.dy映射_Session = jgy由映射号获得映射(z.iy映射号_i);
                if (z.dy映射_Session == null) {
                    z.client.close();
                    selectionKey.cancel();
                } else if (z.dy映射_Session.iv正在服务_b) {
                    try {
                        z.i_byte1s = "{success:false,_:'正在服务......'}".getBytes("UTF-8");
                    } catch (java.io.UnsupportedEncodingException ex) {
                    }
                } else if (!z.dy映射_Session.iIP_s.equals(client.socket().getInetAddress().toString())) {//客户端必需维持握手时的IP
                    z.client.close();
                    selectionKey.cancel();
                } else {
                    ///截去映射号
                    System.arraycopy(z.i_byte1s, 4, z.i_byte1s, 0, z.i_byte1s.length - 4);
                    z.i_byte1s = java.util.Arrays.copyOf(z.i_byte1s, z.i_byte1s.length - 4);

                    z.i_byte1s = jgj解密_byte1s(z.i_byte1s, z.dy映射_Session);
                    if (z.i_byte1s == null) {//解密失败，证明有人捣乱。例如B获取了A的sessionid，利用A的sessionid向服务器申请服务，造成A无法正常申请服务。
                        z.client.close();
                        selectionKey.cancel();
                        return;
                    } else {
                        z.dy映射_Session.ij接收数据量_i += i_byte1s.length;
                        z.dy映射_Session.if访问频率_i++;//访问频率+1
                        z.dy映射_Session.iz最近访问时间_l = java.util.Calendar.getInstance().getTimeInMillis();//设置最近访问时间
                        if (清理(z.dy映射_Session)) {
                            try {
                                z.i_byte1s = "{success:false,_:'请勿攻击服务器！'}".getBytes("UTF-8");
                            } catch (java.io.UnsupportedEncodingException ex) {
                            }
                        } else {
                            z.i_byte1s = jgj解压_byte1s(z.i_byte1s);
                            if (z.i_byte1s == null) {//解压出问题（例如遇到压缩包炸弹）
                                z.i_byte1s = "{success:false,_:'请勿攻击服务器！'}".getBytes("UTF-8");
                            } else {
                                z.iv指令_byte = z.i_byte1s[0];

                                switch (z.iv指令_byte) {
                                    case I.ii申请服务_byte:
                                        //截去系统指令
                                        System.arraycopy(z.i_byte1s, 1, z.i_byte1s, 0, z.i_byte1s.length - 1);
                                        z.i_byte1s = java.util.Arrays.copyOf(z.i_byte1s, z.i_byte1s.length - 1);

                                        z.i_byte1s = jgd响应服务(z.iy映射号_i, z.i_byte1s);

                                        z.i_byte1s = jgy压缩_byte1s(z.i_byte1s);

                                        z.i_byte1s = jgj加密_byte1s(z.i_byte1s, z.dy映射_Session);
                                        break;
//                                case I.iw维持连接_byte:
//                                    z.i_byte1s = jgw维持连接_byte1s(client, z.dy映射_Session);
//                                    break;
//                                case I.it退出_byte:
//                                    z.i_byte1s = jgt退出_byte1s(client, z.dy映射_Session);
//                                    z.client.close();
//                                    selectionKey.cancel();
//                                    return;
                                    default://包含退出
                                        z.i_byte1s = jgt退出_byte1s(client, z.dy映射_Session);
                                        z.client.close();
                                        selectionKey.cancel();
                                        return;
                                }
                            }
                        }
                    }
                }
                I.i发缓冲_Map.put(z.client.toString(), z.i_byte1s);
            } catch (CloneNotSupportedException | java.io.IOException ex) {
                selectionKey.cancel();
//            } catch (ClosedChannelException ex) {
//                Logger.getLogger(负载.class.getName()).log(Level.SEVERE, null, ex);
//                try {
//                    z.client.close();
//                } catch (IOException ex1) {
//                }
//                selectionKey.cancel();
            } finally {
                if (z != null) {
                    z.close();
                }
            }
        }

//        private byte[] jgw维持连接_byte1s(final java.nio.channels.SocketChannel client, final cc.hanzs.server.负载均衡.Session cdy映射_Session) {
//            cdy映射_Session.iz最近访问时间_l = java.util.Calendar.getInstance().getTimeInMillis();
//            cdy映射_Session.if访问频率_i++;
//
//            //增加IP被使用次数
//            String IP_s = client.socket().getInetAddress().toString();
//            jg增加or出少IP次数(IP_s, true);
//
//            return I.Uik空返回值_byte1s;
//        }
        private byte[] jgt退出_byte1s(final java.nio.channels.SocketChannel client, final cc.hanzs.server.负载均衡.Session cdy映射_Session) {
//            //减少IP被使用次数
//            String IP_s = client.socket().getInetAddress().toString();
//            jg增加or出少IP次数(IP_s, false);
//
            cdy映射_Session.if访问频率_i = cc.hanzs.server.负载均衡.Session.if访问频率峰值_i + 1;//添置符合清理的条件，目的是为了尽快清理掉无用session
            清理(cdy映射_Session);

            return I.Uik空返回值_byte1s;
        }

        private byte[] jgd响应服务(final int ci映射号_i, final byte[] ci内容_byte1s) {
            cc.hanzs.server.负载均衡.Session session = I.iy映射_MAP.get(ci映射号_i);
            byte[] ji_byte1s = I.dy业务.g处理请求(ci映射号_i, ci内容_byte1s);
            session.id递回数据量_i += ji_byte1s.length;
            if (清理(session)) {
                try {
                    return "{success:false,_:'请勿攻击服务器！'}".getBytes("UTF-8");
                } catch (java.io.UnsupportedEncodingException ex) {
                    return null;
                }
            } else {
                return ji_byte1s;
            }
        }
    }

    private static final class 清理映射池 extends java.lang.Thread {

        @Override
        public void run() {
            for (;;) {
                try {
                    sleep(cc.hanzs.server.负载均衡.Session.ii时限_l);//60″循环一次
                } catch (InterruptedException ex) {
                }

                Object[] keys;

                //清理session
                keys = I.iy映射_MAP.keySet().toArray();
                for (Object key : keys) {
                    cc.hanzs.server.负载均衡.Session session = I.iy映射_MAP.get(key);
                    if (session != null && !清理(session)) {
                        session.if访问频率_i -= Session.if访问频率递减梯度_i;
                        session.ij接收数据量_i -= Session.ij接收数据量递减梯度_i;
                        session.id递回数据量_i -= Session.id递回数据量递减梯度_i;
                    }
                }

                Session.IP峰值_i = I.dy业务.get用户数量_i() * 2;//设定IP使用次数的峰值，预防同一IP不停的与服务器握手，造成DOS攻击。默认注册用户数量的2倍。
//
//                //清理IP
//                keys = I.IP_MAP.keySet().toArray();
//                for (Object key : keys) {
//                    jg增加or出少IP次数((String) key, false);
//                }
            }
        }
    }

    private static synchronized void jg增加or减少IP次数(final String IP_s, final boolean ci_b) {
        int IP_i = 0;
        Integer IP_I = I.IP_MAP.get(IP_s);
        if (IP_I != null) {
            IP_i = IP_I;
        }

        if (ci_b) {//增加IP被使用次数
            IP_i++;
        } else if (IP_i > 0) {//减少IP被使用次数
            IP_i--;
        }

        if (IP_i <= 0) {//若此IP已无人使用，则移除此IP
            I.IP_MAP.remove(IP_s);
        } else {
            I.IP_MAP.put(IP_s, IP_i);
        }
    }

    private static boolean 清理(final cc.hanzs.server.负载均衡.Session session) {
        long id当前时间_l = java.util.Calendar.getInstance().getTimeInMillis();
        if (id当前时间_l - session.iz最近访问时间_l > cc.hanzs.server.负载均衡.Session.ii时限_l
                || session.if访问频率_i > cc.hanzs.server.负载均衡.Session.if访问频率峰值_i
                || session.ij接收数据量_i > cc.hanzs.server.负载均衡.Session.ij接收数据量峰值_i
                || session.id递回数据量_i > cc.hanzs.server.负载均衡.Session.id递回数据量峰值_i) {

            I.iy映射_MAP.remove(session.iy映射号_i);
            I.dy业务.removeSession(session.iy映射号_i);

            jg增加or减少IP次数(session.iIP_s, false);

            return true;
        } else {
            return false;
        }
    }
}
