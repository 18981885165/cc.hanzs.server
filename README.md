#cc.hanzs.server
##.
###介绍
1、此项目为服务端框架，<BR>
2、配合cc.hanzs.clent使用。<BR>
3、不支持web。<BR>
4、写此项目的目的是建立精简、高效、安全的网路连接服务。<BR>
5、服务端与客户端之间采用了类似TLS的方式。因无法考证，不知是在TLS之前还是之後，我是在写完这个通信框架之後接触到TLS的。<BR>
6、限制单一IP地址频繁访问，阀值是注册用户的2倍。好处是预防DOS攻击，但带来的风险是转而伪造企业公网IP与服务器握手，造成企业无法正常访问云服务。<BR>
7、对DDOS、窃取数据在应用层面做了预防处理，降低了被攻击的风险、窃取数据的速度。单一客户端受到如下限制：<BR>
　　1）只能串行访问。<BR>
　　2）客户端只承认握手时的IP。<BR>
　　3）访问频率受到限制，超出峰值频率，则断开对客户端的服务，默认峰值50次。包含心跳服务，此次客户端默认心跳服务1次/分钟。服务端会每分钟递减访问频率，以保障服务器正常提供服务。<BR>
　　4）向服务器发送的数据量受到限制，超出峰值数据量，则断开对客户端的服务，默认峰值9600。以防客户端向服务端发送大量数据，实测当中，NIO模式下XP系统一次最大只能发送9600。<BR>
　　5）向服务器请求的数据量受到限制，超出峰值数据量，则断开对客户端的服务，默认峰值12000。以防客户端频繁请求返回数据量大的服务。<BR>
　　6）向服务器发送的压缩包的压缩率受限，预防压缩包炸弹攻击。<BR>
8、不能预防下面问题<BR>
　　1）同时大量数据涌入，服务器忙于接收数据，阻塞服务器。应用层只接收前（注册用户数量*2）个连接的数据，单次接收的容量是9600字节，攻陷阀值不清楚。<BR>
　　2）控制肉鸡与服务器握手，利用rsa算法消耗服务器资源。当肉鸡数量足够多时，会造成服务器忙于解密。每个IP的阀值是注册用户数量的2倍，被攻陷的阀值是多少不清楚。<BR>
　　3）如果是云服务器，伪造企业公网IP与服务器不停的握手，虽然不能攻陷服务器，但也能让企业无法访问服务器，因为服务器会认为受到攻击而拒绝服务，将正常的访问也过滤掉了。<BR>
　　4）除了基础协议先天问题外，还有就是认为失误、客户端被攻击。例如员工离职，未在系统内注销该员工的权限；客户端被攻陷，用户被当作肉鸡或口令被窃取等等。<BR>
9、此框架可用于非web领域。如管理软件，或类似的环境，实现业务与web的分离。
##.
###所用编辑器
NetBeans、JAVA8。<BR>
`lib.7z`解压到`lib`文件夹即可，为所需jar。<BR>
`cc.hanzs.server.7z`解压後可得编译并优化後的jar。
##.
###遵循协议
为尽量保障作者的辛劳与顾客的使用，此项目遵循LGPLv3协议：<BR>`1、对软件本身可以修改，但任何人不得将软件本身及修改版用于商业行为。`<BR>`2、任何人对软件的使用（调用、封装，包括修改後的）可免费用于商业软件。`<BR>`3、对软件的技术支持，技术支持者可收取费用。`
##.
###使用方法
```java  
public class 优化入口 extends cc.hanzs.server.业务.__ {

    public static void main(String[] args) {
        try {
            //配置负载参數
            cc.hanzs.server.负载均衡.Property.i中心端口_i = 8080;


            //此rsa针对java客户端，可将密钥长度提高到很长
            cc.hanzs.server.负载均衡.Property.rsa = cc.hanzs.安全.RSA.d副本(1);
            cc.hanzs.server.负载均衡.Property.rsa.g置私钥(cc.hanzs.编码.Base64.d解码.decode("MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAvnU1UYcd93QJv9VLZuCAPONa9QBL5HaiJEkmecnAZZl0QUsrnVYIUF9uIdQu538YVHhFgdXpgF+PKWQ6NkLcfwIDAQABAkEAoYQQlXU6qpHTesXNzg7xcbF1f3Si57rbwl3urTW1XfKNuoRj9eNW1hvii67V61/6TTdqTQ/VBqVSMNB7sOLQQQIhAPo+JJY78fhXr3uWJ0Hvp8GjVk3kOU6Pm5IDx/5SO/AxAiEAwtbzHMDqjJHuIvLrm5q0nB6Os7WzhAlcWyhX3gecm68CIFUpFsxlxBfux1J4rA19YIq+al2BoJsCtj+/t6zuEWXRAiEAqlYfPoENzQz7YwR6pJPw2BgC6+urYICKhItVnh+Lc7MCIHhkYzuaBPF9DrMj4sNwxiQCxrRHC2iXVmI/5fVw/QWE"));
//            cc.hanzs.server.负载均衡.Property.rsa.g置公钥(cc.hanzs.编码.Base64.d解码.decode("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAL51NVGHHfd0Cb/VS2bggDzjWvUAS+R2oiRJJnnJwGWZdEFLK51WCFBfbiHULud/GFR4RYHV6YBfjylkOjZC3H8CAwEAAQ=="));

            //此rsa针对c#客户端，固定长度为1024
            //JAVA:
            //公钥:MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRhc5tk4nJzI3WcDZNDuUjtrVF2IHpBUeW5FMMwdrEYlzqIMjOny4f2GshlDqKfl21Xevj3y0/khhCeKLQp+5cDAafKDXj39GZmG7HKeWZp+DC8AErVYsQCRaXAJuzuowmmunbOa4XoOBfF3aG8o54vz1Tty2MfZpDAur0mUdWbwIDAQAB
            //私钥:MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJGFzm2TicnMjdZwNk0O5SO2tUXYgekFR5bkUwzB2sRiXOogyM6fLh/YayGUOop+XbVd6+PfLT+SGEJ4otCn7lwMBp8oNePf0ZmYbscp5Zmn4MLwAStVixAJFpcAm7O6jCaa6ds5rheg4F8Xdobyjni/PVO3LYx9mkMC6vSZR1ZvAgMBAAECgYAUbIEyY9ic1/a4N/uHCSYjwwriCKcC+1i21sjJK5P2qA9+VKeaSEEC+y7eAZ3NvjqMr+fhjXseRvTcr226dzc+wcGsxkJGWq3uTQa1od4yY1Mu5SRX7OnQ1wnYuIxhgt3+pMtjCe/JbAAZOd+GyDp4t2LJdwPRwOLKzIGmuBb+4QJBAMhxrG0UuQDGreviM1yIwrK/OFNN4EJunGWyrmi23YWxvBIFiGV0IEXRxpXKW/lAMGyQ3q2z9NQjfq/vrkq7EVkCQQC52zzELHNDkHzR3yow5SeEXfyafrdrAyGe+UM0KyOZyUkAvHj6VV7z7Kl8JAzsBdZFU/ujhJeysGNgYlSs6CUHAkEAkI5qa8CmK4kdpSBdKj73KzH8zRc8xKxA3pcecSZwXBryJ40V8RTusWu4bA6khf1k7ucLsPo3d0ah9wBEEmEXAQJAVJ1gq/uEupT0juQoJDyf4h6cG4ZnYRv62ZKoKv8c5S4Dw4cLGQV3BlyPyQnsrCP4KnYS0Z5TJjIx/DaoUFQcTwJAPBkI6jnZ5WzhfGmLfjqzsteICdY3OAO+E+Bsxu2MU3JFGXctu+MW1G+M2+2qZ4jJOsz3WQTY77ZOaH78Ja6kHA==
            //
            //C#:
            //公钥为：AQAB
            //模为：kYXObZOJycyN1nA2TQ7lI7a1RdiB6QVHluRTDMHaxGJc6iDIzp8uH9hrIZQ6in5dtV3r498tP5IYQnii0KfuXAwGnyg149/RmZhuxynlmafgwvABK1WLEAkWlwCbs7qMJprp2zmuF6DgXxd2hvKOeL89U7ctjH2aQwLq9JlHVm8=
            cc.hanzs.server.负载均衡.Property.RsaForDot = cc.hanzs.安全.RSA.d副本(2);
            cc.hanzs.server.负载均衡.Property.RsaForDot.g置私钥(cc.hanzs.编码.Base64.d解码.decode("MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJGFzm2TicnMjdZwNk0O5SO2tUXYgekFR5bkUwzB2sRiXOogyM6fLh/YayGUOop+XbVd6+PfLT+SGEJ4otCn7lwMBp8oNePf0ZmYbscp5Zmn4MLwAStVixAJFpcAm7O6jCaa6ds5rheg4F8Xdobyjni/PVO3LYx9mkMC6vSZR1ZvAgMBAAECgYAUbIEyY9ic1/a4N/uHCSYjwwriCKcC+1i21sjJK5P2qA9+VKeaSEEC+y7eAZ3NvjqMr+fhjXseRvTcr226dzc+wcGsxkJGWq3uTQa1od4yY1Mu5SRX7OnQ1wnYuIxhgt3+pMtjCe/JbAAZOd+GyDp4t2LJdwPRwOLKzIGmuBb+4QJBAMhxrG0UuQDGreviM1yIwrK/OFNN4EJunGWyrmi23YWxvBIFiGV0IEXRxpXKW/lAMGyQ3q2z9NQjfq/vrkq7EVkCQQC52zzELHNDkHzR3yow5SeEXfyafrdrAyGe+UM0KyOZyUkAvHj6VV7z7Kl8JAzsBdZFU/ujhJeysGNgYlSs6CUHAkEAkI5qa8CmK4kdpSBdKj73KzH8zRc8xKxA3pcecSZwXBryJ40V8RTusWu4bA6khf1k7ucLsPo3d0ah9wBEEmEXAQJAVJ1gq/uEupT0juQoJDyf4h6cG4ZnYRv62ZKoKv8c5S4Dw4cLGQV3BlyPyQnsrCP4KnYS0Z5TJjIx/DaoUFQcTwJAPBkI6jnZ5WzhfGmLfjqzsteICdY3OAO+E+Bsxu2MU3JFGXctu+MW1G+M2+2qZ4jJOsz3WQTY77ZOaH78Ja6kHA=="));
//            cc.hanzs.server.负载均衡.Property.rsa.g置公钥(cc.hanzs.编码.Base64.d解码.decode("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRhc5tk4nJzI3WcDZNDuUjtrVF2IHpBUeW5FMMwdrEYlzqIMjOny4f2GshlDqKfl21Xevj3y0/khhCeKLQp+5cDAafKDXj39GZmG7HKeWZp+DC8AErVYsQCRaXAJuzuowmmunbOa4XoOBfF3aG8o54vz1Tty2MfZpDAur0mUdWbwIDAQAB"));

            //启动负载均衡
            cc.hanzs.server.负载均衡.负载.init(new 优化入口());
            cc.hanzs.server.负载均衡.负载.start();
            System.out.println("启动完毕");
        } catch (java.io.IOException | java.security.NoSuchAlgorithmException | java.security.spec.InvalidKeySpecException ex) {
        }
    }

    @Override
    public byte[] g处理请求(final int session_i, final byte[] ci客户数据_bytes) {
        cc.hanzs.json.JSONObject ji_JSON = cc.hanzs.json.JSONObject.d副本("{success:true,_:'测试'}");
        ji_JSON.put("@", session_i);
        return ji_JSON.toString().getBytes();
    }

    @Override
    public void removeSession(int session_i) {
    }
}
```
`私钥`与`公钥`由用户自己生成，`公钥`发布给客户端使用。
